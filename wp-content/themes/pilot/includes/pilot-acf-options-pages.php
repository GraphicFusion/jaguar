<?php
/* 
 * Options Page
 * Add ACF items to an options page 
 * */

if( function_exists('acf_add_options_page') ) {
 
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Footer',
        'menu_title'    => 'Footer',
        'menu_slug'     => 'footer',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Main Menu',
        'menu_title'    => 'Main Menu',
        'menu_slug'     => 'header',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
 
}
?>