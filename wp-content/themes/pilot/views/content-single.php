<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
<div style="" class="block-generic_content module    " id="generic_content_block_0"><div class="layout-content"><div class="container-fluid container-md container-sm">
	<div class="row">
		<div class="col-lg-12">
			<header class="entry-header">
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
			</header>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="gc-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div></div><!--/layout-content--></div>
		<?php echo do_shortcode('[mason_build_blocks container=content]');?>
	</div><!-- .entry-content -->
</article>