jQuery(document).ready(function($) {
	var maxHeight = $('.navigation').outerHeight();
	//$('body').css('padding-top',maxHeight);
	document.addEventListener('scroll', function (event) {
		if(!$('body').hasClass('open-mobile')){
			var headerTrigger = 150,
				footerTrigger = 600,
				ydistance = $(window).scrollTop();
			if(ydistance > headerTrigger){
				$('body').addClass('stuck-header');
				var height = $('.navigation').outerHeight();
			}
			else{
				$('body').removeClass('stuck-header');
				var height = maxHeight;
			}
			if(ydistance > footerTrigger){
				$('body').addClass('show-reveal');			
			}
			else{
				$('body').removeClass('show-reveal');			
			}
			//$('body').css('padding-top',height);
		}
	}, true /*Capture event*/);
	function setFooterDrawdown(){
		var height = ($('article .module').last().outerHeight())/2;
		//$('footer.site-footer').css('padding-top',height).css('margin-top',-height);
	}
//	setFooterDrawdown();	
	$(window).on('resize', function(){
///		setFooterDrawdown();
	})
});
		function revealBio($this, e){
			if(e){
				var target = $(e.target).closest('.member');
			}
			else{
				var target = $this;
			}
				if( $(target).hasClass('active') ){
					$('.member.shadow-card').removeClass('active');
				 	$('.popup-content').removeClass('active');		
				 	//alert(1);			
				}
				else{
					var teamObj = $(target).closest('.team-section').data('id');
					console.log(  'taregt team section ' + teamObj, $(target).closest('.team-section') );
//alert(teamObj);
			 		$('.member.shadow-card').removeClass('active');
			 		var member_id = $(target).data('member'),
			 		member = teams[teamObj][member_id];
			 		//console.log('TEAM:', <?php echo $args['id']; ?>);
			 		console.log('memebr:' + member, member_id, member);
			 		$('.popup-content').removeClass('active');
			 		var $popup = $(target).siblings('.popup-content');
			 		$($popup).addClass('active');
			 		$popup.find('.name').html(member.name);
			 		$popup.find('.title').html(member.title);
			 		$popup.find('.text').html(member.text);
			 		$popup.addClass('active');
			 	}
		}

		function wrapTeam( $parent ){
			$parent.find(' .popup-content').unwrap();
			$parent.find('.popup-content').remove();
			
		 	var divs = $parent.find(".member"),
		 	width = $(window).width();
		 	$incr = 3;
		 	if(width <= 1259 ){
		 		$incr = 2;
		 	}
		 	if(width <= 959 ){
		 		$incr = 1;
		 	}
		 	for(var i = 0; i < divs.length; i+=$incr) {
  				divs.slice(i, i+$incr).wrapAll("<div class='member-row' style='width:100%''></div>");
			}
			$parent.find('.member-row ').append('<div class="popup-content shadow-card"><div class="popup-wrapper container-md container-lg container-fluid container-card"><div class="content-wrapper"><h5 class="name"></h5><p class="title"></p><p class="text"></p></div></div></div>');
			$.each( $parent.find('.member.shadow-card'), function(c,v){
				revealBio(v);
			})
			$parent.find('.member.shadow-card').removeClass('active');
		 	$('.popup-content').removeClass('active');
		}
