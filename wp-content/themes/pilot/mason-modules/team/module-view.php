<?php
	global $args;
	//print_r($args);
?>
<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='team-section' id="section_<?php echo $args['id']; ?>" data-id="<?php echo $args['id']; ?>">
		<div class='team-content-section interior-box-wide container-fluid container-sm container-md container-lg'>
			<div class="row">
			<script>
				var <?php echo $args['id']; ?> = {};
			</script>
				<?php $i = 1; foreach( $args['rows'] as $row ) : ?>
					<script>
						if( 'undefined' == typeof teams ){
							var teams = {};
						}
						if( 'undefined'== typeof teams['<?php echo $args['id']; ?>'] ){
							teams['<?php echo $args['id']; ?>'] = {};
						}
						var member<?php echo $i; ?> = {};
						member<?php echo $i; ?>.name = '<?php echo $row['name']; ?>';
						member<?php echo $i; ?>.text = '<?php echo json_encode(preg_replace("#'#","\'",$row['text'])); ?>';
						member<?php echo $i; ?>.title = '<?php echo $row['title']; ?>'; 
						teams['<?php echo $args['id']; ?>'][<?php echo $i; ?>] = member<?php echo $i; ?>;
						console.log(teams);
					</script>
					<div class='member shadow-card col-xs-12 col-md-6 col-lg-4' data-member='<?php echo $i; ?>'>
							<div class='team-block-image'>
								<img src='<?php echo $row['image']['url']; ?>'>
							</div>
							<div class="member-content">
								<div class="content-wrapper">
									<h5 class="member-name"><?php echo $row['name']; ?></h5>
									<div class="member-title"><?php echo $row['title']; ?></div>
								</div>
							</div>
					</div><!--team-content-section--->
				<?php $i++; endforeach; ?>
			</div>
  		</div><!--/team-section--->
	</div>
	<script>
		$(document).ready(function() {
			$(window).on('resize', function(){
				var $parent = $('#section_<?php echo $args['id']; ?>');
				wrapTeam( $parent );
			});
			var $parent = $('#section_<?php echo $args['id']; ?>');
		 	wrapTeam( $parent );

		 	$parent.find('.member.shadow-card').on('click',function( e ){
		 		revealBio( this, e );
		 	});
		 });
	</script>
<?php endif; ?>