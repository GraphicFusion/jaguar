<?php
	$filename = dirname(__FILE__) . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_team_layout(){
		$rows_arr = mason_get_sub_field('team_block_rows');
		if( is_array($rows_arr) ):
			$rows = array();
			foreach( $rows_arr as $row_arr){
				$rows[] = array(
					'name' => $row_arr['team_block_name'],
					'title' => $row_arr['team_block_title'],
					'text' => $row_arr['team_block_text'],
					'image' => $row_arr['team_block_image']
				);
				
			}
			while ( mason_have_rows('team_block_rows') ) : the_row();
				$rows[] = array(
					'name' => $row_arr['team_block_name'],
					'title' => get_sub_field('team_block_title'),
					'text' => get_sub_field('team_block_text'),
					'image' => get_sub_field('team_block_image')
				);
			endwhile;
			$args['rows'] = $rows;
		endif;
		$args['module_styles'] = [];
		if(get_sub_field('team_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('team_block_padding-top');
		}
		if(get_sub_field('team_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('team_block_padding-bottom');
		}
		
		return $args;
	}

?>