<?php
	function build_divider_layout(){
		$args = [];
		$args['image'] = mason_get_sub_field('divider_block_image');
		$args['module_styles'] = [];
		if(get_sub_field('divider_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('divider_block_padding-top');
		}
		if(get_sub_field('divider_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('divider_block_padding-bottom');
		}
		
		return $args;
	}

?>