<?php global $args; ?>
<div class="container-card container-fluid container-lg container-md container-sm">
	<div class="row">
<?php
	if(count($args['cards']) > 0 ) :
		$i = 0;
		foreach($args['cards'] as $card):  ?>
			<div class="shadow-card " >
				<div class="img-block-wrap" id="<?php echo $args['id']; ?>">
					<?php if($card['bg_image_url']) : ?>
						<div class="bg-image" style="background-image: url(' <?php echo $card['bg_image_url']; ?>');">
						</div><!--/bg-img-->
					<?php endif; ?>
				</div><!--img-block-wrap-->
				<div class="card-foot-content">
						<div class="title-wrap">
							<?php if( $card['title']  ) : ?>
								<h6><?php echo $card['title']; ?></h6>
							<?php endif; ?>
							<?php if(  $card['content']  ) : ?>
								<div class="content"><?php echo $card['content']; ?></div>
							<?php endif; ?>
							<?php if( $card['button']  ) : ?>
									<a class="inde-btn" href="<?php echo $card['button']['url']; ?>" target="<?php echo $card['button']['target']; ?>"><?php echo $card['button']['title']; ?>
									</a>
							<?php endif; ?>
						</div><!--/title-wrap-->
				</div><!--/card-foot-content-->
			</div><!--/shadow-card-->
		<?php endforeach; ?>
	<?php endif; ?>
	</div>
</div>