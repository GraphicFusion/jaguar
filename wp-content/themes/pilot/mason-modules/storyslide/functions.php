<?php
	function build_storyslide_layout( $params ){
		global $i;
		$incr = 1;

		$slides = mason_get_sub_field('storyslide_block_slides');
		$args = [
			'slides' => []
		];
		$args['module_classes']['height_class'] = mason_get_sub_field('storyslide_block_height') ? 'media-tall' : 'media-short';
		if(is_array($slides) && count($slides)>0){
			foreach($slides as $slide){
				$slide['storyslide_block_slide_id'] = 'storyslide_block_slide_'.$i.'_#'.$incr;
				$incr++;

				$args['slides'][] = $slide;
			}
		}
		$bg_image = mason_get_sub_field('storyslide_block_image');
		$opacity = "";
		$percent = "";
		$color = "";
		if(mason_get_sub_field('storyslide_block_modify')){
			$opacity = (mason_get_sub_field('storyslide_block_overlay_opacity')?mason_get_sub_field('storyslide_block_overlay_opacity') : "");
			$percent = (mason_get_sub_field('storyslide_block_overlay_percent')?mason_get_sub_field('storyslide_block_overlay_percent') : "100");
			$color = (mason_get_sub_field('storyslide_block_overlay_color')?mason_get_sub_field('storyslide_block_overlay_color') : "");
		}

		$bg_image_arr = [
			'src' => ($bg_image ? $bg_image['url'] : ""),
			'opacity' => $opacity,
			'percent' => $percent,
			'color' => $color,
			'mp4' => mason_get_sub_field('storyslide_block_bg_video_file_mp4'),
		];
		$args['bg_image'] = $bg_image_arr;
		$args['module_styles'] = [];
		if(get_sub_field('storyslide_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('storyslide_block_padding-top');
		}
		if(get_sub_field('storyslide_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('storyslide_block_padding-bottom');
		}
		return $args;
	}

?>