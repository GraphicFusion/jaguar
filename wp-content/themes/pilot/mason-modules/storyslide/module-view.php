<?php
	global $args;
	$block_name = 'storyslide_block_';	
	$block_id = $args['acf_incr'];
    unset($args['acf_incr']);
    $bg = $args['bg_image'];
?>
<div  style="">
		<?php if( isset( $bg['mp4'] ) || isset( $bg['src']  ) ) : ?>
			<div class="img-block-wrap video"  
                    <?php if($bg['src'] && !$bg['mp4']) : ?>
                        style="background-image: url(<?php echo $bg['src']; ?>);"
                    <?php endif; ?>>
                <?php if($bg['mp4']) : ?>
                    <video loop muted autoplay poster="<?php // echo $arg[$block_name . 'image']['sizes']['large']; ?>" class="fullscreen-bg_video">
                        <source src="<?php echo $bg['mp4']['url']; ?>" type="video/mp4">
                    </video>                
                <?php endif; ?>
					<div class="img-overlay"> 
                        <div class="filter" 
                        style="background-image:linear-gradient(to top right, <?php echo $bg['color']; ?> <?php echo ($bg['percent'] ? $bg['percent'] : 100); ?>%, transparent ); opacity: <?php echo $bg['opacity']; ?>;"></div>
                        <div class="slide-navigation">
                            <div class="container-fluid container-card container-lg">
                                <?php $i = 0; foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; ?>
                                    <div class="row">

                                        <div class="col-xs-5">
                                            <?php if( isset( $arg[$block_name . 'title'] ) && $arg[$block_name . 'title'] ) : ?>        
                                                <div class="<?php if(!$i){ echo "main"; } ?> slide-nav" data-slide="<?php echo $i; ?>">
                                                    <h2 class="slide-title"><?php echo $arg[$block_name . 'title']; ?></h2>
                                                </div>                                                                
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-xs-7"></div>
                                    </div>
                                <?php $i++; endforeach; ?>
                            </div>
                        </div>

                        <div class="media-container" id="slick-slider-<?php echo $block_id; ?>" >
    <?php foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; ?>
                            <div class="slide-content" id="<?php echo $slide_id; ?>">
                                <div class="lower-content">
                                    <div class="container-fluid container-card container-lg">
                                        <div class="row">
                                            <div class="col-md-4"></div>

                                            <div class="col-md-8 col-xs-12 subtitle">

                                                <?php if( isset( $arg[$block_name . 'subtitle'] ) && $arg[$block_name . 'subtitle'] ) : ?>                            
                                                    <p><?php echo $arg[$block_name . 'subtitle']; ?></p>
                                                <?php endif; ?>
                                                <div class="button">

                                                    <?php if(  $arg[$block_name . 'button'] ) : ?>

                                                        <a class="inde-btn" href="<?php echo $arg[$block_name . 'button']['url']; ?>"><?php echo $arg[$block_name . 'button']['title']; ?></a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--/lower-content-->
                            </div>
    <?php endforeach; ?>

                        </div><!--/media-container-->
					</div><!--/img-overlay-->
				</div><!--/img-block-wrap-->
		<?php endif; ?>
</div><!--/slick-slider-->

    <script>
        jQuery(document).ready(function($){
            $('.video-btn').magnificPopup({
                type: 'iframe'
            });
            $('.media-container').css('opacity',1);
            $('.slide-nav').click(function(e){
                var $target = $(e.target);
                var $slide = $target.data('slide');
                $('.slide-nav').removeClass('main');
                if( !$slide ){
                    $target = $(e.target).closest('.slide-nav');
                    $slide = $target.data('slide');
                }
                $target.addClass('main');
                $('#slick-slider-<?php echo $block_id; ?>').slick('slickGoTo',$slide);
            }) ;      

        <?php if(count($args['slides'])>0) : ?>
          $('#slick-slider-<?php echo $block_id; ?>').slick({
            slidesToShow:1,
            dots:false,
            arrows:false,
            autoplay: true,
            autoplaySpeed: 6000
          });
          $('#slick-slider-<?php echo $block_id; ?>').on('afterChange', function(event, slick, currentSlide, nextSlide){
                $('.slide-nav').removeClass('main');
                $('.slide-nav[data-slide=' + currentSlide + ']').addClass('main');

  console.log('slide',currentSlide);
  if(!currentSlide){
    //$('#slick-slider-<?php echo $block_id; ?>').slickSetOption("autoplay",false,false);
  }
});
        <?php endif; ?>

        });

      
    </script>

