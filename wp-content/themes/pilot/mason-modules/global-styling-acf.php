<?php
	function get_styling_fields( $module ){
		$included_modules = [
			'media',
			'slideshow'
		];
		if(1 == 1){
			$module = $module . "_block_";
			$fields =  array(
				array(
					'key' => create_key($module,'module_styling'),
					'label' => 'Styling Parameters',
					'name' => $module.'_module_styling',
					'type' => 'true_false',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => 'Hide Styles',
					'ui_off_text' => 'Show Styles',
				),
				array(
					'key' => create_key($module,'top_padding'),
					'label' => 'Top Padding',
					'name' => $module.'padding-top',
					'type' => 'text',
					'instructions' => 'Add padding to Module as: "10%" or "10px"',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => create_key($module,'bottom_padding'),
					'label' => 'Bottom Padding',
					'name' => $module.'padding-bottom',
					'type' => 'text',
					'instructions' => 'Add padding to Module as: "10%" or "10px"',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => create_key($module,'module_styling'),
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '30%',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			);
			return $fields;
		}
	}
?>