<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 * array	$args['rows']			 //  array of rows of lede/content
	 * string	$args['rows'][0]['lede'] //  row title
	 * string	$args['rows'][0]['hidden_content'] //  row hidden content
	 */
	global $args; 
?>
<?php if( (is_array($args['rows']) && count($args['rows']) > 0 )) : ?>
<div class="container-fluid container-md container-sm">
	<div class="row">
		<div class="accordion-wrapper col-lg-12">
			<?php if( count($args['rows']) > 0 ) : ?>
				<dl class="accordion">
					<?php if( is_array( $args['rows'] ) ) : ?>
						<?php foreach( $args['rows'] as $row ): ?>
							<dt>
								<div class="row">
									<div class="col-xs-8">
										<h5><?php echo $row['lede']; ?></h5>
									</div>
									<div class="col-xs-4">
										<?php if ($row['hidden_content']) : ?>
											<div class="acc-tog">
												<div class="horizontal"></div>
												<div class="vertical"></div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</dt>
							<dd>
								<div class="row">
									<div class="col-xs-12 content">
										<?php echo $row['hidden_content']; ?>
									</div>
								</div>
							</dd>
						<?php endforeach; ?>
					<?php endif; ?>
				</dl>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>
<script>
	jQuery(document).ready(function($) {
	// Accordion script
	
	var allPanels = $('.accordion > dd').hide(),
		allPanelLabels = $('.accordion > dt');
		
	$('.accordion > dt .acc-tog').click(function() {
		if ( !$(this).closest('dt').hasClass('active js-open') ) {
			allPanels.slideUp();
			allPanelLabels.removeClass('active js-open');
			$(this).closest('dt').next().slideDown();
			$(this).closest('dt').addClass('active js-open');
		}
		else {
			$(this).closest('dt').removeClass('active js-open').next().slideUp();
		}
		return false;
	});
});

</script>