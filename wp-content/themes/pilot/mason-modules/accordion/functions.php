<?php
	function build_accordion_layout(){
		$args = array(
			'title' => mason_get_sub_field('accordion_block_title'),
			'content' => mason_get_sub_field('accordion_block_content'),
			'rows' => mason_get_sub_field('accordion_block_rows')
		);
		$args['module_styles'] = [];
		if(get_sub_field('accordion_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('accordion_block_padding-top');
		}
		if(get_sub_field('accordion_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('accordion_block_padding-bottom');
		}


		return $args;
	}
?>