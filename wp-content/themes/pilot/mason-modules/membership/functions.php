<?php
	function build_membership_layout(){
		$args = array(
			'button' => mason_get_sub_field('membership_block_button'),
			'caption' => mason_get_sub_field('membership_block_block_caption'),
			'title_one' => mason_get_sub_field('membership_block_title_one'),
			'rows_one' => mason_get_sub_field('membership_block_rows_one'),
			'title_two' => mason_get_sub_field('membership_block_title_two'),
			'rows_two' => mason_get_sub_field('membership_block_rows_two'),
			'title_three' => mason_get_sub_field('membership_block_title_three'),
			'rows_three' => mason_get_sub_field('membership_block_rows_three'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('membership_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('membership_block_padding-top');
		}
		if(get_sub_field('membership_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('membership_block_padding-bottom');
		}
		
		return $args;
	}
?>