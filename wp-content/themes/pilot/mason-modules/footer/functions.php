<?php
	function build_footer_layout(){
		$args = array(
			'logo' => mason_get_sub_field('footer_block_logo'),
			'image' => mason_get_sub_field('footer_block_reveal_image'),
			'title' => mason_get_sub_field('footer_block_title'),
			'subtitle' => mason_get_sub_field('footer_block_subtitle'),
			'email' => mason_get_sub_field('footer_block_email'),
			'button' => mason_get_sub_field('footer_button'),
			'links' => mason_get_sub_field('footer_links'),
			'social' => mason_get_sub_field('footer_social'),
			'info' => mason_get_sub_field('footer_info')			
		);
		return $args;
	}
?>