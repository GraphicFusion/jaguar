<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>
<div class="navigation">
	<div class="container-fluid container-lg">
		<div class="row">
			<div class="col-xs-2">
				<div class="logo-wrapper">			
					<a href="/">
						<img class="logo" src="<?php echo $args['logo']['url']; ?>">
						<!--<div class="secondary-logo-wrapper">
							<img class="secondary-logo" src="<?php echo $args['secondary_logo']['url']; ?>">
						</div>-->
					</a>
				</div>
			</div>
			<div class="col-xs-9">
				<div class="special-nav">
						<?php if(array_key_exists('special', $args) && is_array($args['special'])) : ?>
							<?php foreach($args['special'] as $arr) : $spec = $arr['navigation_block_special_link']; ?>
								<?php if(is_array($spec) && array_key_exists('url', $spec)) : ?>
								<a class="inde-btn" href="<?php echo $spec['url']; ?>" target="<?php echo $spec['target']; ?>"><?php echo $spec['title']; ?></a> 
							<?php endif; ?>
							<?php endforeach; ?>	
						<?php endif; ?>

				</div>
			</div>
				<div class="col-xs-1" id="mobile-nav-wrapper">
					<div class="mobile-nav">
						<div class="menu-button">
							<img class="menu-open menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-MENU-BLACK.svg">
						</div>
					</div>
				</div>						
		</div>

	</div>
		<div class="row tagline">
			<div class="col-xs-1"></div>

			<div class="col-xs-10 ">
				<h6 class="tag">You are invited on a sacred quest to save the jaguar, heal the land, and help the people!</h6>
			</div>
		</div>
</div>
<div class="mobile-navigation">
	<div class="mobile-menu">
		<div class="menu-close-wrapper-close">
			<div class="menu-button-close">
				<img class="menu-close menu-blue" src="<?php echo get_template_directory_uri(); ?>/mason-modules/navigation/images/ICON-CLOSE-GRAY.svg">						
			</div>
		</div>
		<div class="container-fluid container-lg container-md container-sm container-card">	
			<?php if( count($args['main_links'])>0): ?>
				<div class="primary-nav row">
					<?php foreach($args['main_links'] as $main): 
						$main_href = $main['navigation_block_main_link']['url'];
						$active_class = "";
						if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
							$active_class = " active";
						}
					?>
						<?php $subs = ""; if( is_array($main['navigation_block_sublinks']) && count($main['navigation_block_sublinks'])>0): ?>
							<?php foreach($main['navigation_block_sublinks'] as $sub ) : 
								if( isset($sub['navigation_block_sub_link']['url'])){
									$sub_href = $sub['navigation_block_sub_link']['url'];
									if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
										$active_class = " active";
									}
									$subs .= '<li><a href="'.$sub_href.'">'.$sub['navigation_block_sub_link']['title'].'</a></li>';
								}
							endforeach; ?>
						<?php endif; ?>
						<div class="menu-item-wrapper col-xs-12 col-sm-6 col-lg-2 <?php echo $active_class; ?>">
							<div class="rule"></div>
							<a href="<?php echo $main_href; ?>">
								<?php echo $main['navigation_block_main_link']['title']; ?>
							</a>
							<?php if($subs) : ?>
								<ul class="sub-nav">
									<?php echo $subs; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>

			<div class="mobile-and-footer row" style="padding-top:30px;">
				<div class="mf-col menu-item-wrapper col-xs-12 col-sm-6 col-lg-6"><p class="small"><?php echo $args['directions']; ?></p></div>
				<div class="mf-col menu-item-wrapper col-xs-12 col-sm-6 col-lg-3">
					<p class="small">
						<?php if(array_key_exists('extra', $args)) : ?>
							<?php foreach($args['extra'] as $arr) : $extra = $arr['navigation_block_extra_link']; ?>
								<?php if(array_key_exists('url', $args)) : ?>
								<a href="<?php echo $extra['url']; ?>" target="<?php echo $extra['target']; ?>"><?php echo $extra['title']; ?></a> 
							<?php endif; ?>
							<?php endforeach; ?>	
						<?php endif; ?>
					</p>
				</div>
				<div class="mf-col menu-item-wrapper col-xs-12 col-sm-6 col-lg-3"><p class="small">
					<?php if(array_key_exists('social', $args) && is_array($args['social'])) : ?>
						<?php foreach($args['social'] as $arr) : 
							$logo = $arr['navigation_block_social_logo']; 
							$link = $arr['navigation_block_social_link']; ?>
							<a class="social" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
								<img src="<?php echo $logo['url']; ?>">
							</a> 
						<?php endforeach; ?>				
					<?php endif; ?>
				</p></div>
			</div>
		</div>
	</div>
</div><!--/what-->

<script>
	function getScrollBarWidth () {
	    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
	        widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
	    $outer.remove();
	    return 100 - widthWithScroll;
	};
	$( document ).ready(function() {
		var sb = getScrollBarWidth();
		$('.menu-button,.menu-button-close').click(function(){
			if( $('.block-navigation').hasClass('open-mobile') ){
				$('.block-navigation').removeClass('open-mobile');
				$('body').removeClass('open-mobile');
				//alert(window.scrollPosition);
				window.scrollTo(0,window.scrollPosition);
			}
			else{
				$('.block-navigation').addClass('open-mobile');
				$('body').addClass('open-mobile');
				var scrollPosition = $(document).scrollTop();
				console.log(scrollPosition);
				window.scrollPosition = scrollPosition;
			//	alert(window.scrollPosition);
				window.scrollTo(0,0);
			}
		});
		var $footer_menu = $(".mobile-navigation").clone().appendTo( $('.footer-content') );
		$footer_menu.find('.menu-close-wrapper-close').remove();
		$footer_menu.find('.container-fluid.container-lg').removeClass('container-card');
		function threeUpSlider(){
			var width = $(window).width();
			if(width < 1260 ){ 
				$('.block-card .row').slick({
					arrows:false,
					variableWidth:true
				});
				var footerPos = $('.site-footer .mobile-menu ').offset();
				//$('.row.slick-slider').css('padding-left',footerPos.left);
			}
			else{
				$('.block-card .row').slick({
					variableWidth:true,
					slidesToShow:3
				});

			}
			console.log( footerPos );
			$.each($('.shadow-card .img-block-wrap'), function(k,v){
				var width = $(v).width(),
					height = width * 2/3;
					$(v).height(height);
					$(v).find('.bg-image').height(height);
			})
		}
		threeUpSlider();
		$(window).on('resize', function(){
			threeUpSlider();
		});
	});
</script>