<?php
	global $pilot;
	// add module layout to flexible content 
	$key = "textAction";
	$name = $key . "_block_";
	$module_layout = array (
		'key' => create_key('textAction','block'),
		'name' => 'textAction_block',
		'label' => 'Call To Action (Text) Block',
		'display' => 'block',
		'sub_fields' => array (
					array(
						'key' =>  create_key($key, 'max_width'),
						'label' => 'Max Width of Content (in px)',
						'name' => $name . 'max_width',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '25%',
							'class' => '',
							'id' => '',
						),
						'message' => '',
					),
					array(
						'key' => create_key($key,'background_color'),
						'label' => 'Background Color',
						'name' => $name.'background_color',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'choices' => array(
							'white' => 'White',
							'dark-bg' => 'Dark Blue',
							'light-bg' => 'Light Blue',
							'yellow-bg' => 'Yellow',
							'clear' => 'Transparent',

						),
						'default_value' => array(
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'return_format' => 'value',
						'ajax' => 0,
						'placeholder' => '',
						'conditional_logic' => array(),
						'wrapper' => array(
							'width' => '25%',
							'class' => '',
							'id' => '',
						),
					),
					array(
						'key' =>  create_key($key, 'content'),
						'label' => 'Content (Left Side)',
						'name' => 'textAction_block_content',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'full',
						'media_upload' => 1,
						'delay' => 0,
					),
					array(
						'key' =>  create_key($key, 'button'),
						'label' => 'Button (Right Side)',
						'name' => 'textAction_block_button',
						'type' => 'link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '50%',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',

					),
					array(
						'key' =>  create_key($key, 'secondbutton'),
						'label' => 'Second Button (Optional)',
						'name' => 'textAction_block_secondbutton',
						'type' => 'link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '50%',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',

					),
                    array(
						'key' =>  create_key($key, 'button_class'),
						'label' => 'Button Class',
						'name' => 'textAction_block_button_class',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '50%',
							'class' => '',
							'id' => '',
						),

					),
				),
		'min' => '',
		'max' => '',
	);
?>