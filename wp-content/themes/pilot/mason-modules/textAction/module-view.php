<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */

	global $args;
	$row_class = "row ";
	$row_style = "";
	if($args['max_width']){
		//$row_class .= " tfcu-container";
		$row_style = "style='max-width:". $args['max_width'] . "px'";
	}
?>
<div class="container">
	<div class="<?php echo $row_class;?>" <?php echo $row_style; ?>>
		<div class="col-lg-6" >
			<div class="col-content">
				<?php echo $args['content']; ?>
			</div>
		</div><!--/col-->
		<div class="col-lg-6 no-gutter" >
			<div class="col-content right">
				<?php if($args['button']) : ?>
					<a class="inde-btn btn-blue btn <?php echo $args['button_class']; ?>" href="<?php echo $args['button']['url']; ?>"><?php echo $args['button']['title']; ?></a>
				<?php endif; ?>
				<?php if($args['secondbutton']) : ?>
					<a style="margin-left:30px;" class="inde-btn btn-blue btn <?php echo $args['secondbutton_class']; ?>" href="<?php echo $args['secondbutton']['url']; ?>"><?php echo $args['secondbutton']['title']; ?></a>
				<?php endif; ?>				
			</div>
		</div><!--/col-->
	</div><!--/row-->
</div>