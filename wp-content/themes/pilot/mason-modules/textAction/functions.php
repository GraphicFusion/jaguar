<?php
	function build_textAction_layout(){
		$args = array(
			'content' => mason_get_sub_field('textAction_block_content'),
			'button' => mason_get_sub_field('textAction_block_button'),
			'secondbutton' => mason_get_sub_field('textAction_block_secondbutton'),
            'button_class' => mason_get_sub_field('textAction_block_button_class'),
			'block_hr' => mason_get_sub_field('textAction_block_block_hr'),
			'max_width' => mason_get_sub_field('textAction_block_max_width'),
		);
		$args['custom_classes'] = [];
		$args['classes'] = mason_get_sub_field('textAction_block_background_color');
		if(get_sub_field('textAction_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('textAction_block_padding-top');
		}
		if(get_sub_field('textAction_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('textAction_block_padding-bottom');
		}
		return $args;
	}
?>