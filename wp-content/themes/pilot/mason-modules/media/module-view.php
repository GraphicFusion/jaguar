<?php
	global $args;
	$block_name = 'media_block_';	
	$block_id = $args['acf_incr'];
    unset($args['acf_incr']);
?>
<div id="slick-slider-<?php echo $block_id; ?>" style="">
	<?php foreach ( $args['slides'] as $arg ) : $slide_id = $arg[$block_name . "slide_id"]; ?>
		<?php if( isset( $arg[$block_name . 'bg_video_file_mp4'] ) || isset( $arg[$block_name . 'image']  ) ) : ?>
			<div class="img-block-wrap video" id="<?php echo $slide_id; ?>" 
                    <?php if($arg[$block_name . 'image'] && !$arg[$block_name . 'bg_video_file_mp4']) : ?>
                        style="background-image: url(<?php echo $arg[$block_name . 'image']['sizes']['large']; ?>);"
                    <?php endif; ?>>
                <?php if($arg[$block_name . 'bg_video_file_mp4']) : ?>
                    <video loop muted autoplay poster="<?php // echo $arg[$block_name . 'image']['sizes']['large']; ?>" class="fullscreen-bg_video">
                        <source src="<?php echo $arg[$block_name . 'bg_video_file_mp4']['url']; ?>" type="video/mp4">
                    </video>                
                <?php endif; ?>
					<div class="img-overlay"> 
                        <div class="filter" 
                        style="background-image:linear-gradient(to top right, <?php echo $arg[$block_name . 'overlay_color']; ?> <?php echo ($arg[$block_name . 'overlay_percent'] ? $arg[$block_name . 'overlay_percent'] : 100); ?>%, transparent ); opacity: <?php echo $arg[$block_name . 'overlay_opacity']; ?>;"></div>

                        <div class="media-container" >
                            <div class="slide-content">
                                <div class="lower-content">
                                    <div class="container-fluid container-card container-lg">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                            <?php if( isset( $arg[$block_name . 'title'] ) && $arg[$block_name . 'title'] ) : ?>                            
                                                <h1><?php echo $arg[$block_name . 'title']; ?></h1>
                                            <?php endif; ?>
                                            </div><div class="col-xs-12 col-md-6"></div>
                                        </div>
                                        <div class="row">

                                            <div class="col-xs-6 subtitle">

                                                <?php if( isset( $arg[$block_name . 'subtitle'] ) && $arg[$block_name . 'subtitle'] ) : ?>                            
                                                    <p><?php echo $arg[$block_name . 'subtitle']; ?></p>
                                                <?php endif; ?>

                                            </div><div class="col-xs-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <?php if(  $arg[$block_name . 'video_file_mp4'] ||  $arg[$block_name . 'youtube'] ) :
                                                    $video = ($arg[$block_name . 'youtube'] ? $arg[$block_name . 'youtube'] : $arg[$block_name . 'video_file_mp4']['url'] )
                                                 ?>
                                                    <a class="video-btn " href="<?php echo $video; ?>">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
                                                            <path d="M20,0C9,0,0,9,0,20s9,20,20,20c11,0,20-9,20-20S31,0,20,0z M15.4,30V10l13.3,10L15.4,30z" />
                                                        </svg>
                                                    </a>
                                                <?php endif; ?>
                                                <?php if(  $arg[$block_name . 'button'] ) : ?>

                                                    <a class="inde-btn" href="<?php echo $arg[$block_name . 'button']['url']; ?>"><?php echo $arg[$block_name . 'button']['title']; ?></a>
                                                <?php endif; ?>
                                            </div>
                                        </div>  <!--/row-->                                      
                                    </div>
                                </div><!--/lower-content-->
                            </div>
                        </div><!--/media-container-->
					</div><!--/img-overlay-->
				</div><!--/img-block-wrap-->
		<?php endif; ?>
	<?php endforeach; ?>
</div><!--/slick-slider-->

    <script>
        jQuery(document).ready(function($){
            $('.video-btn').magnificPopup({
          type: 'iframe'
      });

window.mobileVideo = function (){
        var winWidth = $(window).width();

/*        if(winWidth < 1020){
            $.each($('.img-block-wrap'),function(k,v){

                var bg_image = $(v).data('poster');
                $(v).closest('.img-block-wrap').find('video').remove();
                $(v).closest('.img-block-wrap').css('background-image', 'url(' + bg_image + ')');

            })      
        }
        */
    }            
            var dots = false;
        <?php if(count($args['slides'])>0) : ?>
            $('#slick-slider-<?php echo $block_id; ?>').on('init', function(e, slick, direction){
                $('.slick-dots').wrap( "<div class='col-lg-12'></div>" );
                $('.slick-dots').closest('.col-lg-12').wrap( "<div class='row'></div>" );
                $('.slick-dots').closest('.row').wrap( "<div class='container'></div>" );
                $('.slick-dots').closest('.container').wrap( "<div class='slick-dots-wrapper'></div>" );
                $('.slick-dots-wrapper').wrap("<div class='slick-dots-div'></div>" );
            });

        <?php endif; ?>
        <?php if(count($args['slides'])>0) : ?>
          $('#slick-slider-<?php echo $block_id; ?>').slick({
            slidesToShow:1,
            dots:false,
            arrows:false,
            adaptiveHeight: true
          });
        <?php endif; ?>

        });

      
    </script>

