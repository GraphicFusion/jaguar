<?php
	function build_generic_content_layout(){
		$args = array(
			'isHeader' => mason_get_sub_field('generic_content_block_isHeader'),
			'title' => mason_get_sub_field('generic_content_block_title'),
			'use_card' => mason_get_sub_field('generic_content_block_use_card'),
			'content' => mason_get_sub_field('generic_content_block_content'),
		);
		$args['module_styles'] = [];
		if(get_sub_field('generic_content_block_padding-top')){
			$args['module_styles']['padding-top'] = get_sub_field('generic_content_block_padding-top');
		}
		if(get_sub_field('generic_content_block_padding-bottom')){
			$args['module_styles']['padding-bottom'] = get_sub_field('generic_content_block_padding-bottom');
		}

		return $args;
	}
?>